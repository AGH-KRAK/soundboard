module Soundboard.Wav (
      save
    , load
    ) where

import qualified Data.WAVE as WAV

import Soundboard.Types(TFR, Frequency(Frequency))
import Soundboard.Wavelet(dwt', idwt')

load :: String -> IO (WAV.WAVEHeader, [TFR])
load fileName = WAV.getWAVEFile fileName >>= \wave ->
    let header = WAV.waveHeader wave
        samplingRate = Frequency . fromIntegral $ WAV.waveFrameRate header
    in return 
       (header, dwt' samplingRate . fmap WAV.sampleToDouble <$> WAV.waveSamples wave)

save :: String -> WAV.WAVEHeader -> [TFR] -> IO ()
save fileName header tfrs = 
    let tracks = (fmap.fmap) WAV.doubleToSample $ idwt' <$> tfrs
    in WAV.putWAVEFile fileName $ WAV.WAVE header tracks

