{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Soundboard.Types (
    -- * Main types
      TFR
    , Spectrum
        ( SNote
        , SComposite
        , Spectrum
        )
    -- * Real numbers
    , Time(Time)
    , Frequency(Frequency)
    , Amplitude(Amplitude)
    , Response(Response)
    -- * Re-exports
    , Complex((:+))
    ) where
import Data.Complex(Complex((:+)))


newtype Time = Time Double deriving (Eq, Ord, Show, Read, Enum, Floating, Fractional, Num, Real, RealFloat, RealFrac)
newtype Frequency = Frequency Double deriving (Eq, Ord, Show, Read, Enum, Floating, Fractional, Num, Real, RealFloat, RealFrac)
newtype Amplitude = Amplitude Double deriving (Eq, Ord, Show, Read, Enum, Floating, Fractional, Num, Real, RealFloat, RealFrac)
newtype Response = Response (Complex Double) deriving (Eq, Floating, Fractional, Num, Show, Read)


-- | A mapping of the frequency of sine waves to their amplitude and offset. In denotational terms, 
-- μ Spectrum = Frequency -> Complex Double
--
-- Various constructors can be used depending on the source of the signal - an audio file vs a synthesizer
data Spectrum = 
    -- | Represents a series of sinusoidal waves.
    -- Each overtone has the frequency of the fundamental *2, *3, *4, etc
      SNote
        { fundamental :: (Frequency, Response)
        -- | Overtone amplitude is relative to the fundamental.
        , overtones :: [Amplitude]
        } 
    -- | Allows for sum
    -- μ Composite = fmap (+)
    | SComposite Spectrum Spectrum
    -- | Holds data imported from an audio file. 
    -- In the denotation, values between samples are linearly interpolated.
  -- DO WE STORE THIS AS:  | SDiscrete :: Map Frequency (Complex Double)
  -- OR AS:
    | Spectrum (Frequency -> Response) -- mapped over all frequencies, lerped and extrapolated

type TFR = Time -> Spectrum






