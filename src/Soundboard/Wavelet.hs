module Soundboard.Wavelet (
    -- * Types
      Wavelet
    -- * Discrete Transforms
    , dwt
    , dwt'
    , idwt
    , idwt'
    -- * Wavelet generation
    , morlet
    ) where

import Soundboard.Types(Time(..), Frequency(..), Response(..), Spectrum(..), TFR, Complex(..))

type Wavelet = Time -> Response

-- | Discrete Wavelet Transform
dwt :: Wavelet    -- ^ The shape of the "mother" wavelet, assumed to be normalized at 1Hz with center at t=0
    -> Frequency  -- ^ The sampling frequency, eg. 44.1KHz
    -> [Double]   -- ^ The data stream to transform
    -> TFR        -- ^ The resulting time frequency representation
dwt wavelet samplingRate stream = \t -> Spectrum $ \f -> 
    let Frequency scale = samplingRate / f
        Time offset = t
    in
    -- √|scale| * ∫ ψ((τ-offset)/scale) signal(τ) dτ
    (Response . complex . sqrt . abs) scale 
    * integral
      (zip [1..] stream)
      (\(t', signal_t') -> 
        let scaledTime = Time $ (t' - offset) / scale
        in wavelet scaledTime * (Response . complex) signal_t'
      )
    -- todo: optimize so we only calculate the significant part of the wavelet
    --   - it approaches zero very quickly on either side

-- | Discrete Wavelet Transform with "good-enough" parameters
dwt' :: Frequency -- ^ The sampling frequency
     -> [Double]
     -> TFR
dwt' = dwt morlet'

-- | Inverse Discrete Wavelet Transform
idwt :: Wavelet 
     -> TFR 
     -> [Double]
idwt = undefined

-- | Inverse Discrete Wavelet Transform with "good-enough" parameters
idwt' :: TFR -> [Double]
idwt' = idwt morlet'

-- | A 1Hz complex morlet wavelet, with a variable gaussian window
-- Larger bandwidths give better certainty in frequency.
-- Smaller bandwidths give better certainty in time.
morlet :: Double  -- ^ The bandwidth parameter (how wide the gaussian window is)
       -> Wavelet
morlet bandwidth (Time t) = Response $
    complex (exp (-(t**2) / bandwidth)) *  -- gaussian component
    exp (0 :+ tau * t)         -- pure spiral with a period of 1

-- | A morlet wavelet with default parameters
morlet' :: Wavelet
morlet' = morlet 2.0

-- Helpers

tau :: Floating a => a
tau = 2 * pi

integral :: Num b => [a] -> (a -> b) -> b
integral l f = sum $ map f l

complex :: Num a => a -> Complex a
complex x = x :+ 0

