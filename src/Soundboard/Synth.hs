module Soundboard.Synth (
      sine
    , square
    , saw
    ) where

import Soundboard.Types(Spectrum)

sine :: Spectrum
sine = undefined

square :: Spectrum
square = undefined
-- square freq val = Note freq val overtones
--  where overtones = (\i -> if i `mod` 2 == 0 then 0 else 1/i) <$> [1..]

saw :: Spectrum
saw = undefined
